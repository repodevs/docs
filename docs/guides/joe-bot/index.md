---
title: How to work with Joe chatbot
sidebar_label: Overview
slug: /guides/joe-bot
---

## Guides
- [How to get a query execution plan (EXPLAIN)](/docs/guides/joe-bot/get-query-plan)
- [How to create an index using Joe bot](/docs/guides/joe-bot/create-index)
- [How to reset the state of a Joe session / clone](/docs/guides/joe-bot/reset-session)
- [How to get a list of active queries in a Joe session and stop long-running queries](/docs/guides/joe-bot/query-activity-and-termination)
- [How to visualize a query plan](/docs/guides/joe-bot/visualize-query-plan)
- [How to work with SQL optimization history](/docs/guides/joe-bot/sql-optimization-history)
- [How to get row counts for arbitrary SELECTs](/docs/guides/joe-bot/count-rows)
- [How to get sizes of PostgreSQL databases, tables, and indexes with psql commands](/docs/guides/joe-bot/get-database-table-index-size)

## Related
- [Joe chat commands reference](/docs/joe-bot/commands-reference)

[↵ Back to Guides](/docs/guides/)
