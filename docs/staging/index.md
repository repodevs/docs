---
title: Staging with superpowers
sidebar_label: Overview
slug: /staging
---

Develop and test using full-size database clones provisioned in seconds to get most reliable results much faster.
Save disk space. A lot. Local thin clones share the majority of data blocks ("copy-on-write") saving time and budgets.
Tasks such as verification of major upgrades, partitioning, index maintenance can and must be verified in Database Lab first, to minimize risks of negative events in production.

:::note
This page is unfinished. Reach out to the Postgres.ai team to learn more.
:::